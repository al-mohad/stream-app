import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Video Stream')),
      body: Column(
        children: [
          AspectRatio(
            aspectRatio: 16 / 9,
            child: Chewie(controller: controller.chewieController),
          ),
          const SizedBox(height: 20),
          Obx(
            () => controller.stitching
                ? (controller.stichedChewieController != null
                    ? AspectRatio(
                        aspectRatio: 16 / 9,
                        child: Chewie(
                            controller: controller.stichedChewieController!),
                      )
                    : const Center(
                        child: CircularProgressIndicator(),
                      ))
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}
