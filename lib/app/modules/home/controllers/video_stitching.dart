import 'dart:io';
import 'dart:isolate';

void stitchSegments(List<String> segmentPaths, String outputPath) {
  final stitchedFile = File(outputPath);
  final outputSink = stitchedFile.openWrite();

  for (final segmentPath in segmentPaths) {
    final segmentFile = File(segmentPath);
    final segmentBytes = segmentFile.readAsBytesSync();
    outputSink.add(segmentBytes);
  }

  outputSink.close();
}

void stitchSegmentsInIsolate(SendPort sendPort) {
  ReceivePort receivePort = ReceivePort();
  sendPort.send(receivePort.sendPort);

  receivePort.listen((dynamic message) {
    final segmentPaths = message[0] as List<String>;
    final outputPath = message[1] as String;

    stitchSegments(segmentPaths, outputPath);

    sendPort.send('Stitching completed');
  });
}
