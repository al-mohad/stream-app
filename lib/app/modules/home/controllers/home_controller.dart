import 'dart:io';

import 'package:chewie/chewie.dart';
import 'package:dio/dio.dart';
import 'package:ffmpeg_kit_flutter/ffmpeg_kit.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_player/video_player.dart';

class HomeController extends GetxController {
  ChewieController? stichedChewieController;
  late ChewieController chewieController;
  VideoPlayerController? stitchedVideoController;
  final videoPlayerController = VideoPlayerController.network(
      'https://dc5jkysis23ep.cloudfront.net/streams/e9a35b6f9d0bc631f4afd283ff606cbc-6/playlist.m3u8');
  final _stitching = false.obs;
  bool get stitching => _stitching.value;
  final _status = ''.obs;
  String get status => _status.value;

  @override
  void onInit() {
    print('onInit');
    _downloadVideo();

    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      aspectRatio: 3 / 2,
      autoPlay: false,
      looping: true,
    );
    super.onInit();
  }

  @override
  void onReady() {
    print('onReady');
    // fetchContents();
    super.onReady();
  }

  @override
  void onClose() {
    print('onClose');
    stitchedVideoController?.dispose();
    super.onClose();
  }

  Future<void> _downloadVideo() async {
    _stitching.value = true;
    Get.log(isError: true, 'Downloading...');
    final dio = Dio();
    final response = await dio.get(
      'https://dc5jkysis23ep.cloudfront.net/streams/e9a35b6f9d0bc631f4afd283ff606cbc-6/playlist.m3u8',
      options: Options(responseType: ResponseType.bytes),
    );
    print('*****************');
    print(response);

    final tempDir = await getTemporaryDirectory();
    // Save the video track to a local file
    final file = File('${tempDir.path}/video.m3u8');
    await file.writeAsBytes(response.data);

    // Use the downloaded video track for stitching
    await _stitchVideo(file);
  }

// Inside your class...

  Future<void> _stitchVideo(File videoTrackFile) async {
    // Get the temporary directory
    final tempDir = await getApplicationSupportDirectory();
    final outputFilePath = '${tempDir.path}/noor.mp4';

    final command =
        '-f concat -safe 0 -i ${videoTrackFile.path} -c copy $outputFilePath';

    // Execute the FFmpeg command
    final returnCode = await FFmpegKit.execute(command);

    if (returnCode.isFFmpeg()) {
      // Stitching completed successfully
      Get.log('Video stitching completed! Output: $outputFilePath');
      stitchedVideoController =
          VideoPlayerController.file(File(outputFilePath));
      stichedChewieController = ChewieController(
        videoPlayerController: stitchedVideoController!,
        aspectRatio: 3 / 2,
        autoPlay: true,
        autoInitialize: true,
        looping: true,
      );
      // Use the stitched video for playback or further processing
    } else {
      // Stitching failed
      Get.log('Video stitching failed!');
      // Handle the failure scenario appropriately
    }
    stichedChewieController!.isPlaying ? _stitching.value = false : true;
  }
}
